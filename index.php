<html>
    <head>
        <title>Iniciar sesión</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="CSS/Estilos.css" rel="stylesheet">

    </head>
    <body>
        <div class="loginContendor">
            <img class="imagen" src="images/ic_login.png" alt="icono Login">
            <h1>Inicio de sesión</h1>

            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                <!--Nombre de usuario-->

                <div class="inputBox">
                    <input type="text" name="usuario" required autofocus>
                    <label for="nombreUsuario">Usuario o correo electrónico</label>
                </div>
                <!--Password-->
                <div class="inputBox">

                    <input id="contra" type="password" name="password" required>
                    <img id="imagen" class="icono" src="icons/eyeClose.png">
                    <label for="contraseña">Contraseña</label>
                </div>

                <input type="submit" value="Iniciar" name="iniciarSesion">
            </form>

        </div>
        <script src="js/jquery.min.js" type="text/javascript"></script>

        <script>
            $('#imagen').click(mostrarContra);

            function mostrarContra() {
                var pass = document.getElementById("contra");
                if (pass.type == 'password') {
                    $('#imagen').attr("src", "icons/eyeOpen.png");
                    pass.type = 'text';
                    pass.focus();
                } else {
                    $('#imagen').attr("src", "icons/eyeClose.png");
                    pass.type = 'password';
                    pass.focus();
                }
            }
        </script>
    </body>
</html>
