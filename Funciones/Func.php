<?php
function isNull($nombre, $user, $pass, $pass_con, $email, $apellido)
{
    if (strlen(trim($nombre)) < 1 || strlen(trim($user)) < 1 || strlen(trim($pass)) < 1 || strlen(trim($pass_con)) < 1 || strlen(trim($email)) < 1 || strlen(trim($apellido)) < 1) {
        return true;
    } else {
        return false;
    }
}
function editarPass($pass_hash, $idUser) {

    global $conexion;

    $stmt = $conexion->prepare("UPDATE usuarios SET contraseña=? WHERE idUser=?");
    $stmt->bind_param('si',  $pass_hash, $idUser);


    if ($stmt->execute()) {
        return $conexion;
    } else {
        return 0;
    }
}

function isEmail($email)
{
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true;
    } else {
        return false;
    }
}

function validaPassword($var1, $var2)
{
    if (strcmp($var1, $var2) !== 0) {
        return false;
    } else {
        return true;
    }
}

function minMax($min, $max, $valor)
{
    if (strlen(trim($valor)) < $min) {
        return true;
    } else if (strlen(trim($valor)) > $max) {
        return true;
    } else {
        return false;
    }
}

function usuarioExiste($usuario)
{
    global $conexion;

    $stmt = $conexion->prepare("SELECT idUser FROM usuarios WHERE usuario = ? LIMIT 1");
    $stmt->bind_param("s", $usuario);
    $stmt->execute();
    $stmt->store_result();
    $num = $stmt->num_rows;
    $stmt->close();

    if ($num > 0) {
        return true;
    } else {
        return false;
    }
}

function emailExiste($email)
{
    global $conexion;

    $stmt = $conexion->prepare("SELECT idUser FROM usuarios WHERE correo = ? LIMIT 1");
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $stmt->store_result();
    $num = $stmt->num_rows;
    $stmt->close();

    if ($num > 0) {
        return true;
    } else {
        return false;
    }
}


function hashPassword($password)
{
    $hash = password_hash($password, PASSWORD_DEFAULT);
    return $hash;
}

function resultBlock($errors)
{
    $class = "";
    if (count($errors) > 0) {
        if (in_array("Registro insertado correctamente", $errors)) {
            $class = "class='alert-success'";
        } else {
            $class = "class='alert-danger'";
        }
        echo "<div id='error'" . $class . ">
			<a href='#'id='ocultar' onclick=\"cerrarDivErrores();\" class='closeEx' >&times;</a><ul style='margin-top: -4%;'>";

        foreach ($errors as $error) {
            echo "<li>" . $error . "</li>";
        }
        echo "</ul>";
        echo "</div>";
    }
}

function resultBlockLogin($errors)
{


    if (count($errors) > 0) {
        echo "<div class='clean-gray'>";
        foreach ($errors as $error) {
            echo "$error";
        }
        echo "</div> <br>";
    }

}

function registraUsuario($usuario, $pass_hash, $nombre, $email, $tipo_usuario, $estado, $apellido, $nameFoto, $rutaFoto)
{

    global $conexion;
    try {
        $stmt = $conexion->prepare("INSERT INTO usuarios (usuario, contraseña, nombre, correo, id_tipo, Estado, apellido, nombreFoto, rutaFoto) VALUES(?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('ssssiisss', $usuario, $pass_hash, $nombre, $email, $tipo_usuario, $estado, $apellido, $nameFoto, $rutaFoto);

        if ($stmt->execute()) {
            return $conexion;
        } else {
            return 0;
        }
    } catch (PDOException $ex) {
        echo $ex->getMessage();
    }


}
function registraUsuario1($usuario, $pass_hash, $nombre, $email, $tipo_usuario, $estado, $apellido, $nameFoto, $rutaFoto)
{

    global $conexion;
    try {
        $pst = $conexion->prepare("INSERT INTO usuarios(usuario, contraseña, nombre, correo, id_tipo, Estado, apellido, nombreFoto, rutaFoto) VALUES(:usuario, :contraseña, :nombre,
        :correo, :id_tipo, :Estado, :apellido, :nombreFoto, :rutaFoto)");
        $pst->bind_param(":usuario",$usuario,PDO::PARAM_STR);
        $pst->bind_param(":contraseña",$pass_hash,PDO::PARAM_STR);
        $pst->bind_param(":nombre",$nombre,PDO::PARAM_STR);
        $pst->bind_param(":correo",$email,PDO::PARAM_STR);
        $pst->bind_param(":id_tipo",$tipo_usuario,PDO::PARAM_INT);
        $pst->bind_param(":Estado",$estado,PDO::PARAM_INT);
        $pst->bind_param(":apellido",$apellido,PDO::PARAM_STR);
        $pst->bind_param(":nombreFoto",$nameFoto,PDO::PARAM_STR);
        $pst->bind_param(":rutaFoto",$rutaFoto,PDO::PARAM_STR);
       echo $pst->execute();
    } catch (PDOException $ex) {
        echo $ex->getMessage();
    }

}
function registraUsuario2($usuario, $pass_hash, $nombre, $email, $tipo_usuario, $estado, $apellido, $nameFoto, $rutaFoto)
{

    global $conexion;


}
function activarUsuario($id)
{
    global $conexion;

    $stmt = $conexion->prepare("UPDATE usuarios SET activacion=1 WHERE id = ?");
    $stmt->bind_param('s', $id);
    $result = $stmt->execute();
    $stmt->close();
    return $result;
}

function isNullLogin($usuario, $password)
{
    if (strlen(trim($usuario)) < 1 || strlen(trim($password)) < 1) {
        return true;
    } else {
        return false;
    }
}

function login($usuario, $password)
{
    global $conexion;
    $errors = "";
    $stmt = $conexion->prepare("SELECT idUser, id_tipo, contraseña, usuario FROM usuarios WHERE usuario = ? || correo = ? LIMIT 1 ");
    $stmt->bind_param("ss", $usuario, $usuario);
    $stmt->execute();
    $stmt->store_result();
    $rows = $stmt->num_rows;

    if ($rows > 0) {

        if (isActivo($usuario)) {

            $stmt->bind_result($id, $id_tipo, $passwd, $nombre);
            $stmt->fetch();

            $validaPassw = password_verify($password, $passwd);

            if ($password == $passwd) {

                lastSession($id);
                $_SESSION['id_usuario'] = $id;
                $_SESSION['tipo_usuario'] = $id_tipo;
                $_SESSION['nombre_user'] = $nombre;

                header("location: Ventas.php");
            } else {
                $errors = "La contrase&ntilde;a es incorrecta";
            }
        } else {
            $errors = 'El usuario no está activo, consulte con el administrador';
        }
    } else {
        $errors = "El nombre de usuario o correo electr&oacute;nico no existe";
    }
    return $errors;
}

function lastSession($id)
{
    global $conexion;

    $stmt = $conexion->prepare("UPDATE usuarios SET last_session=NOW() WHERE idUser = ?");
    $stmt->bind_param('s', $id);
    $stmt->execute();
    $stmt->close();
}

function isActivo($usuario)
{
    global $conexion;

    $stmt = $conexion->prepare("SELECT Estado FROM usuarios WHERE usuario =? || correo = ? LIMIT 1");
    $stmt->bind_param('ss', $usuario, $usuario);
    $stmt->execute();
    $stmt->bind_result($activacion);
    $stmt->fetch();

    if ($activacion == 1) {
        return true;
    } else {
        return false;
    }
}


function getValor($campo, $campoWhere, $valor)
{
    global $conexion;

    $stmt = $conexion->prepare("SELECT $campo FROM usuarios WHERE $campoWhere = ? LIMIT 1");
    $stmt->bind_param('s', $valor);
    $stmt->execute();
    $stmt->store_result();
    $num = $stmt->num_rows;

    if ($num > 0) {
        $stmt->bind_result($_campo);
        $stmt->fetch();
        return $_campo;
    } else {
        return null;
    }
}






